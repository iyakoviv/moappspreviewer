package com.example.ok.moappstestpriviewer.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by OK on 04.09.2018.
 */

public enum StorageManager {

    INSTANCE;

    private SharedPreferences mSharedPreferences;

    public void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public void add(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String get(String key) {
        return mSharedPreferences.getString(key, "");
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public void removeAll() {
        mSharedPreferences.edit().clear().commit();
    }
}