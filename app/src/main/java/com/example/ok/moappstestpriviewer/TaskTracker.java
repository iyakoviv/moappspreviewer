package com.example.ok.moappstestpriviewer;

/**
 * Created by OK on 02.09.2018.
 */

public class TaskTracker {

    private int tasksDone;

    private int tasksCount;
    private OnTaskTrackerListener listener;

    public void setTaskCount(int taskCount) {
        this.tasksCount = taskCount;
    }

    public void setTaskListener(OnTaskTrackerListener listener) {
        this.listener = listener;
    }

    public void addTask() {
        this.tasksDone++;
        isFinished();

    }

    private void isFinished() {
        if (tasksCount == tasksDone) {
            listener.onFinished();
        }
    }

    interface OnTaskTrackerListener {
        void onFinished();
    }
}
