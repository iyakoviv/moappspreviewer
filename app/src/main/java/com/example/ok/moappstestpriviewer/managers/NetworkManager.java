package com.example.ok.moappstestpriviewer.managers;

import android.text.TextUtils;

import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.apiservices.apps.AppItemsNetworkService;
import com.example.ok.moappstestpriviewer.apiservices.login.LoginNetworkService;
import com.example.ok.moappstestpriviewer.models.apps.AppItems;
import com.example.ok.moappstestpriviewer.models.login.LoginResponse;

/**
 * Created by OK on 04.09.2018.
 */

public enum NetworkManager {

    INSTANCE;

    // ----- xToken ----- //
    private String xToken;

    private String getXToken() {
        if (TextUtils.isEmpty(xToken)) {
            xToken = (String) StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_X_TOKEN_KEY);
        }
        return xToken;
    }

    private void setXToken(String xToken) {
        StorageManager.INSTANCE.add(StorageConstants.STORAGE_USER_X_TOKEN_KEY, xToken);
        this.xToken = xToken;
    }

    public void login(final String login, final String password,  final NetworkCallback callback) {

        LoginNetworkService.loginUser(login, password, new NetworkCallback() {
            @Override
            public void onSuccess(Object object) {
                LoginResponse loginResponce = (LoginResponse) object;
                if (!TextUtils.isEmpty(loginResponce.getData())) {
                    setXToken(loginResponce.getData());
                }
                StorageManager.INSTANCE.add(StorageConstants.STORAGE_USER_LOGIN_KEY, login);
                StorageManager.INSTANCE.add(StorageConstants.STORAGE_USER_PASSWORD_KEY, password);

                callback.onSuccess(object);
            }

            @Override
            public void onFail(int code) {
                callback.onFail();
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }

    public void getAppData(final Integer skip, final Integer take, final Integer osType, final String userToken, final NetworkCallback callback){
        AppItemsNetworkService.getApps(skip, take, osType, userToken, new NetworkCallback() {
            @Override
            public void onSuccess(Object object) {
                callback.onSuccess(object);
            }

            @Override
            public void onFail(int code) {
                callback.onFail(code);
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }

}
