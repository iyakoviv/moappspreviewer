package com.example.ok.moappstestpriviewer.apiservices;

/**
 * Created by OK on 04.09.2018.
 */

public class NetworkConstants {

    public final static String BASE_URL = "https://html5.mo-apps.com/";

    public final static String API_URL = "api/";

    public final static String LOGIN_URL = "Account/Login";

    public final static String APP_URL = "application";


}
