package com.example.ok.moappstestpriviewer.managers;

import android.util.Log;

import com.example.ok.moappstestpriviewer.models.apps.AppItemData;
import com.example.ok.moappstestpriviewer.models.apps.AppItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by iyakoviv on 04.09.18.
 */

public enum DataManager {

    INSTANCE;

    AppItems appItems;

    List<String> appItemsIds;
    Map<String, AppItemData> appItemDataMap;

    public AppItemData getAppItemDataById(String id){
        if (appItemDataMap != null){
            return appItemDataMap.get(id);
        }
        return null;
    }

    public void fillColletions(AppItems appItems){
        appItemDataMap = new HashMap<>();
        appItemsIds = new ArrayList<>();

        for (int i = 0; i < appItems.getData().size(); i++){
            String appId = appItems.getData().get(i).getApplicationId();
            AppItemData itemData = appItems.getData().get(i);
            appItemDataMap.put(appId, itemData);
            appItemsIds.add(appId);
        }

    }

    public void clear(){
        appItemsIds = new ArrayList<>();
        appItemDataMap = new HashMap<>();
    }

    public AppItems getAppItems() {
        return appItems;
    }

    public void setAppItems(AppItems appItems) {
        this.appItems = appItems;
        fillColletions(appItems);
    }


    public List<String> getAppItemsIds() {
        return appItemsIds;
    }

    public Map<String, AppItemData> getAppItemDataMap() {
        return appItemDataMap;
    }

}
