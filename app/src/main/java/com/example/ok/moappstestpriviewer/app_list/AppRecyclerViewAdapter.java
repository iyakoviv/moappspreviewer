package com.example.ok.moappstestpriviewer.app_list;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ok.moappstestpriviewer.R;
import com.example.ok.moappstestpriviewer.apiservices.MoAppsAPI;
import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.managers.DataManager;
import com.example.ok.moappstestpriviewer.managers.NetworkManager;
import com.example.ok.moappstestpriviewer.managers.StorageConstants;
import com.example.ok.moappstestpriviewer.managers.StorageManager;
import com.example.ok.moappstestpriviewer.models.apps.AppItemData;
import com.example.ok.moappstestpriviewer.models.apps.AppItems;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.ok.moappstestpriviewer.app_list.AppDetailsActivity.APP_URL;

/**
 * Created by iyakoviv on 04.09.18.
 */


public class AppRecyclerViewAdapter extends RecyclerView.Adapter<AppRecyclerViewAdapter.ViewHolder> {


    private Activity activity;
    List<String> appItemsIds;
    Map<String, Integer> appPositionsMap;

    public AppRecyclerViewAdapter(Activity activity, List<String> appItemsIds) {
        this.activity = activity;
        this.appItemsIds = appItemsIds;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if(appPositionsMap == null){
            appPositionsMap = new HashMap<>();
        }

        appPositionsMap.put(appItemsIds.get(position), position);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.context, AppDetailsActivity.class);
                intent.putExtra(AppDetailsActivity.APP_URL, holder.itemData.getApplicationUrl());
                holder.context.startActivity(intent);
            }
        });

        AppItemData appItemData = DataManager.INSTANCE.getAppItemDataById(appItemsIds.get(position));
        if (appItemData != null){
            holder.itemData = appItemData;
            initAppData(holder, position);
        } else {
            NetworkManager.INSTANCE.getAppData(0, 1000, 0, StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_X_TOKEN_KEY), new NetworkCallback() {
                @Override
                public void onSuccess(Object object) {
                    AppItems appItems = (AppItems) object;
                    DataManager.INSTANCE.setAppItems(appItems);
                    AppItemData itemData = DataManager.INSTANCE.getAppItemDataById(appItemsIds.get(position));
                    holder.itemData = itemData;
                    initAppData(holder, position);
                }

                @Override
                public void onFail(int code) {

                }

                @Override
                public void onFail() {

                }
            });
        }

    }

    private void initAppData(final ViewHolder holder, int position){
        
        initAppImage(holder);
        initAppInformation(holder);
    }

    private void initAppInformation(ViewHolder holder) {

        holder.appName.setText(holder.itemData.getApplicationName());

        if (holder.itemData.getPayment()){
            holder.appPaid.setText(R.string.app_list_paid);
        } else {
            holder.appPaid.setText(R.string.app_list_not_paid);
        }

        if (holder.itemData.getApplicationStatus()){
            holder.appFinished.setText(R.string.app_list_finished);
            holder.appFinishedIcon.setImageResource(R.drawable.finished_icon);
        } else {
            holder.appFinished.setText(R.string.app_list_not_finished);
            holder.appFinishedIcon.setImageResource(R.drawable.unfinished_icon);
        }
    }

    private void initAppImage(ViewHolder holder) {
            String imageUrl = holder.itemData.getApplicationIcoUrl();
            if (imageUrl != null || imageUrl.isEmpty()) {
                Picasso.with(holder.context).load(imageUrl).into(holder.appLogo);
            }

    }

    @Override
    public int getItemCount() {
        return appItemsIds.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public Context context;
        public AppItemData itemData;

        @BindView(R.id.app_logo)
        ImageView appLogo;

        @BindView(R.id.app_description_container)
        RelativeLayout appDescriptionContainer;

        @BindView(R.id.app_name)
        TextView appName;
        @BindView(R.id.app_paid_icon)
        ImageView appPaidIcon;
        @BindView(R.id.app_paid_text)
        TextView appPaid;
        @BindView(R.id.app_finished_icon)
        ImageView appFinishedIcon;
        @BindView(R.id.app_finished_text)
        TextView appFinished;

        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            mView = itemView;
            ButterKnife.bind(this, itemView);
        }

    }
}
