package com.example.ok.moappstestpriviewer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.app_list.AppsListActivity;
import com.example.ok.moappstestpriviewer.managers.NetworkManager;
import com.example.ok.moappstestpriviewer.utils.NetworkUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_edittext_login)
    EditText mEdittextLogin;
    @BindView(R.id.login_edittext_password)
    EditText mEdittextPassword;
    @BindView(R.id.login_enter_button)
    Button mEnterButton;
    @BindView(R.id.login_information_button)
    TextView mInformationButton;

    private TaskTracker mTaskTracker;

    private AppProgressDialog progressDialog;


    private void showProgress() {
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mEdittextLogin.setCompoundDrawablePadding(20);
        mEdittextPassword.setCompoundDrawablePadding(20);

        progressDialog = new AppProgressDialog(this);

    }

    @OnClick(R.id.login_enter_button)
    public void login(){
        Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
        final String login = mEdittextLogin.getText().toString().trim();
        final String password = mEdittextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, getString(R.string.login_toast_fill_all_fields), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!NetworkUtils.isOnline(this)) {
            NetworkUtils.noInternetConnectionToast(this);
            return;
        }


        NetworkManager.INSTANCE.login(login, password, new NetworkCallback() {
            @Override
            public void onSuccess(Object object) {
                goToListActivity();
            }

            @Override
            public void onFail(int code) {
                Toast.makeText(LoginActivity.this, getString(R.string.login_error_toast), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail() {
                Toast.makeText(LoginActivity.this, getString(R.string.login_error_toast), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.login_information_button)
    public void information(){
        Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("asd")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void goToListActivity() {
        Intent intent = new Intent(LoginActivity.this, AppsListActivity.class);
        startActivity(intent);
        finish();
    }

}
