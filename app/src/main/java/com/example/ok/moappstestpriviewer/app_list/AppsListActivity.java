package com.example.ok.moappstestpriviewer.app_list;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.ok.moappstestpriviewer.LoginActivity;
import com.example.ok.moappstestpriviewer.MoAppsRecyclerView;
import com.example.ok.moappstestpriviewer.R;
import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.managers.DataManager;
import com.example.ok.moappstestpriviewer.managers.NetworkManager;
import com.example.ok.moappstestpriviewer.managers.StorageConstants;
import com.example.ok.moappstestpriviewer.managers.StorageManager;
import com.example.ok.moappstestpriviewer.models.apps.AppItems;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppsListActivity extends AppCompatActivity {

    @BindView(R.id.app_list)
    MoAppsRecyclerView mAppRecyclerView;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout mAppSwipeRefreshLayout;
    @BindView(R.id.list_empty)
    TextView mListEmpty;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_list);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        initAppList();

        mAppSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initAppList();
                mAppSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initAppList() {

        NetworkManager.INSTANCE.getAppData(0, 1000, 0, StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_X_TOKEN_KEY), new NetworkCallback() {
            @Override
            public void onSuccess(Object object) {
                AppItems appItems = (AppItems) object;
                DataManager.INSTANCE.setAppItems(appItems);
                initAppAdapter(DataManager.INSTANCE.getAppItemsIds());
            }

            @Override
            public void onFail(int code) {

            }

            @Override
            public void onFail() {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_change_user)
        {
            DataManager.INSTANCE.clear();
            StorageManager.INSTANCE.remove(StorageConstants.STORAGE_USER_X_TOKEN_KEY);
            StorageManager.INSTANCE.remove(StorageConstants.STORAGE_USER_LOGIN_KEY);
            StorageManager.INSTANCE.remove(StorageConstants.STORAGE_USER_PASSWORD_KEY);
            goToLoginActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(AppsListActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private void initAppAdapter(List<String> appsIds){
        mAppRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAppRecyclerView.setEmptyView(mListEmpty);
        mAppRecyclerView.setAdapter(new AppRecyclerViewAdapter(this, appsIds));

    }
}
