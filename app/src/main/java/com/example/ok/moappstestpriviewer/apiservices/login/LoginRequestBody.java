package com.example.ok.moappstestpriviewer.apiservices.login;

/**
 * Created by OK on 02.09.2018.
 */

import com.google.gson.annotations.SerializedName;

public class LoginRequestBody {

    @SerializedName("userNick")
    private String userNick;
    @SerializedName("password")
    private String password;

    public String getUserNick() {
        return userNick;
    }

    public String getPassword() {
        return password;
    }

    public LoginRequestBody(String userNick, String password) {
        this.userNick = userNick;
        this.password = password;
    }
}
