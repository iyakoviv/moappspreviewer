package com.example.ok.moappstestpriviewer.apiservices.apps;

import com.example.ok.moappstestpriviewer.apiservices.MoAppsAPI;
import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.apiservices.NetworkConstants;
import com.example.ok.moappstestpriviewer.models.apps.AppItems;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by OK on 04.09.2018.
 */

public class AppItemsNetworkService {

    private static Retrofit retrofit;

    public static void getApps(Integer skip, Integer take, Integer osType, String userToken, final NetworkCallback callback) {

        retrofit = MoAppsAPI.INSTANCE.retrofit();

        AppItemsService appItemsService = retrofit.create(AppItemsService.class);

        AppItemsRequestBody appItemsRequestBody = new AppItemsRequestBody(skip, take, osType, userToken);

        Call<AppItems> call = appItemsService.getItems(appItemsRequestBody);
        call.enqueue(new Callback<AppItems>() {
            @Override
            public void onResponse(Call<AppItems> call, retrofit2.Response<AppItems> response) {
                AppItems appItems = response.body();
                if (appItems != null) {
                    callback.onSuccess(appItems);
                } else {
                    callback.onFail(response.code());
                }
                clear();
            }

            @Override
            public void onFailure(Call<AppItems> call, Throwable t) {
                t.printStackTrace();
                callback.onFail();
                clear();
            }
        });

    }

    private static void clear() {
        retrofit = null;
    }

    public interface AppItemsService {
        @POST(NetworkConstants.APP_URL)
        Call<AppItems> getItems(
                @Body AppItemsRequestBody appItemsRequestBody
        );
    }
}
