package com.example.ok.moappstestpriviewer.apiservices;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by OK on 04.09.2018.
 */

public enum MoAppsAPI {

    INSTANCE;

    private Retrofit xTokenRetforit;
    private Retrofit retrofit;

    private static OkHttpClient createHttpClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
                requestBuilder.method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }

    public Retrofit retrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(NetworkConstants.BASE_URL + NetworkConstants.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public Retrofit xTokenRetforit() {
        if (xTokenRetforit == null) {
            xTokenRetforit = new Retrofit.Builder()
                    .baseUrl(NetworkConstants.BASE_URL + NetworkConstants.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(createHttpClient())
                    .build();
        }
        return xTokenRetforit;

    }

}
