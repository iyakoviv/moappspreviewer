package com.example.ok.moappstestpriviewer.models.apps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iyakoviv on 04.09.18.
 */

public class AppItemData {

    @SerializedName("applicationToken")
    private String applicationToken;
    @SerializedName("isPayment")
    private Boolean isPayment;
    @SerializedName("applicationStatus")
    private Boolean applicationStatus;
    @SerializedName("applicationName")
    private String applicationName;
    @SerializedName("endOfPayment")
    private Object endOfPayment;
    @SerializedName("applicationIcoUrl")
    private String applicationIcoUrl;
    @SerializedName("applicationUrl")
    private String applicationUrl;
    @SerializedName("applicationId")
    private String applicationId;
    @SerializedName("CountOrders")
    private Integer countOrders;
    @SerializedName("CountNewOrders")
    private Integer countNewOrders;
    @SerializedName("CountInProgressOders")
    private Integer countInProgressOders;
    @SerializedName("CountCompletedOrders")
    private Integer countCompletedOrders;


    public String getApplicationToken() {
        return applicationToken;
    }

    public Boolean getPayment() {
        return isPayment;
    }

    public Boolean getApplicationStatus() {
        return applicationStatus;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public Object getEndOfPayment() {
        return endOfPayment;
    }

    public String getApplicationIcoUrl() {
        return applicationIcoUrl;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public Integer getCountOrders() {
        return countOrders;
    }

    public Integer getCountNewOrders() {
        return countNewOrders;
    }

    public Integer getCountInProgressOders() {
        return countInProgressOders;
    }

    public Integer getCountCompletedOrders() {
        return countCompletedOrders;
    }
}
