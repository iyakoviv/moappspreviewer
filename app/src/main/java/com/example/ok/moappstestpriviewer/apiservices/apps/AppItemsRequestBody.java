package com.example.ok.moappstestpriviewer.apiservices.apps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OK on 04.09.2018.
 */

public class AppItemsRequestBody {

    @SerializedName("skip")
    private Integer skip;
    @SerializedName("take")
    private Integer take;
    @SerializedName("osType")
    private Integer osType;
    @SerializedName("userToken")
    private String userToken;

    public Integer getSkip() {
        return skip;
    }

    public Integer getTake() {
        return take;
    }

    public Integer getOsType() {
        return osType;
    }

    public String getUserToken() {
        return userToken;
    }

    public AppItemsRequestBody(Integer skip, Integer take, Integer osType, String userToken) {
        this.skip = skip;
        this.take = take;
        this.osType = osType;
        this.userToken = userToken;
    }
}
