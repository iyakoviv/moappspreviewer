package com.example.ok.moappstestpriviewer.models.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OK on 04.09.2018.
 */

public class LoginResponse {
    @SerializedName("data")
    private String data;
    @SerializedName("err")
    private Boolean err;
    @SerializedName("code")
    private Integer code;

    public String getData() {
        return data;
    }

    public Boolean getErr() {
        return err;
    }

    public Integer getCode() {
        return code;
    }
}
