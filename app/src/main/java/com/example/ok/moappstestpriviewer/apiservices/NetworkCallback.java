package com.example.ok.moappstestpriviewer.apiservices;

/**
 * Created by OK on 04.09.2018.
 */

public interface NetworkCallback {

    public void onSuccess(Object object);

    public void onFail(int code);

    public void onFail();
}