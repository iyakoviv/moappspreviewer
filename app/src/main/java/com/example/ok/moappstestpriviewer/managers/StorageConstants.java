package com.example.ok.moappstestpriviewer.managers;

/**
 * Created by OK on 04.09.2018.
 */

public class StorageConstants {

    public final static String STORAGE_USER_X_TOKEN_KEY = ".STORAGE_USER_X_TOKEN_KEY";
    public final static String STORAGE_USER_LOGIN_KEY = ".STORAGE_USER_LOGIN_KEY";
    public final static String STORAGE_USER_PASSWORD_KEY = ".STORAGE_USER_PASSWORD_KEY";

}
