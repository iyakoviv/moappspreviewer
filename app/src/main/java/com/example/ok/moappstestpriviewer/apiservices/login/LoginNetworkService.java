package com.example.ok.moappstestpriviewer.apiservices.login;

import com.example.ok.moappstestpriviewer.apiservices.MoAppsAPI;
import com.example.ok.moappstestpriviewer.apiservices.NetworkCallback;
import com.example.ok.moappstestpriviewer.apiservices.NetworkConstants;
import com.example.ok.moappstestpriviewer.models.login.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by OK on 02.09.2018.
 */


public class LoginNetworkService {

    private static Retrofit retrofit;

    public static void loginUser(final String userNick, final String password, final NetworkCallback callback) {

        retrofit = MoAppsAPI.INSTANCE.retrofit();

        LoginService loginService = retrofit.create(LoginService.class);

        LoginRequestBody loginRequestBody = new LoginRequestBody(userNick, password);

        Call<LoginResponse> call = loginService.loginUser(loginRequestBody);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                LoginResponse loginResponce = response.body();
                if (!loginResponce.getErr()) {
                    callback.onSuccess(loginResponce);
                } else {
                    callback.onFail(response.code());
                }
                clear();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onFail();
                clear();
            }
        });
    }

    public interface LoginService {
        @POST(NetworkConstants.LOGIN_URL)
        Call<LoginResponse> loginUser(
                @Body LoginRequestBody loginRequestBody
        );
    }

    private static void clear() {
        retrofit = null;
    }
}

