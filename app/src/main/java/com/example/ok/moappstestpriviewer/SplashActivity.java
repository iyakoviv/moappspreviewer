package com.example.ok.moappstestpriviewer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.ok.moappstestpriviewer.app_list.AppsListActivity;
import com.example.ok.moappstestpriviewer.managers.StorageConstants;
import com.example.ok.moappstestpriviewer.managers.StorageManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (!isUserHasLoginData()) {
            goToLoginActivity();
        } else {
            goToAppListActivity();
        }
    }

    private void goToAppListActivity() {
        Intent intent = new Intent(SplashActivity.this, AppsListActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean isUserHasLoginData() {
        String xToken = StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_X_TOKEN_KEY);
        String login = StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_LOGIN_KEY);
        String password = StorageManager.INSTANCE.get(StorageConstants.STORAGE_USER_PASSWORD_KEY);
        return !TextUtils.isEmpty(xToken) && !TextUtils.isEmpty(login) && !TextUtils.isEmpty(password);
    }
}
