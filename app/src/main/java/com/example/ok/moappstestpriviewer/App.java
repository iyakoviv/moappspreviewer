package com.example.ok.moappstestpriviewer;

import android.app.Application;

import com.example.ok.moappstestpriviewer.managers.StorageManager;

/**
 * Created by iyakoviv on 04.09.18.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        StorageManager.INSTANCE.init(this);
    }
}
