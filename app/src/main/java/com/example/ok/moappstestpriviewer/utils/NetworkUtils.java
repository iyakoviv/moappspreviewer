package com.example.ok.moappstestpriviewer.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.example.ok.moappstestpriviewer.R;

/**
 * Created by OK on 04.09.2018.
 */

public class NetworkUtils {

    public static boolean isOnline(Context context) {

        if (context != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }

        return false;
    }

    public static void noInternetConnectionToast(Context context) {
        Toast.makeText(context, context.getString(R.string.toast_no_internet_connection), Toast.LENGTH_SHORT).show();
    }
}