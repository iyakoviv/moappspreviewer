package com.example.ok.moappstestpriviewer.app_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.example.ok.moappstestpriviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppDetailsActivity extends AppCompatActivity {

    public static String APP_URL = "AppDetailsActivity.APP_URL";

    @BindView(R.id.details)
    WebView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_details);

        ButterKnife.bind(this);

        String appUrl = getIntent().getStringExtra(APP_URL);
        details.loadUrl(appUrl);
    }

}
