package com.example.ok.moappstestpriviewer.models.apps;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppItems {

    @SerializedName("data")
    @Expose
    public List<AppItemData> data = null;
    @SerializedName("err")
    @Expose
    public Boolean err;
    @SerializedName("code")
    @Expose
    public Integer code;

    public List<AppItemData> getData() {
        return data;
    }

    public Boolean getErr() {
        return err;
    }

    public Integer getCode() {
        return code;
    }
}